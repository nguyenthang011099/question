##### 1) What is Asynchronous call?

- A function that enables processing to continue without waiting for function to return the value.

##### 2) What is Full-text search ?

- In this we can match a word or phrase in multiple columns and fetch rows accordingly. The Full-text searches are supported for MyISAM tables only
ex. : SELECT * FROM articles
    WHERE MATCH (title,body) AGAINST ('database');
	
##### 3) Can we use cross domain URL in Ajax?

- Yes. We can use "JSONP" ie. JSON with padding to to request data from a server in a different domain.

```javascript
$.ajax({
     url:"testserver.php",
     dataType: 'jsonp', // Notice! JSONP <-- P (lowercase)
     success:function(json){
         // do stuff with json (in this case an array)
         alert("Success");
     },
     error:function(){
         alert("Error");
     },
});
```
##### 4) What are design patterns in PHP

- Singleton
- Front Controller
- Factory Pattern
- Model-View-Controller
- Repository Pattern

##### 5) What is encapsulation in PHP?

- Encapsulation is just wrapping some data in an object. The term "encapsulation" is often used interchangeably with "information hiding".
- Encapsulation is a way of storing an object or data as a property within another object, so that the outer object has full control over what how the internal data or object can be accessed.
- Encapsulation is the process of hidding the data of the object from outside world and access to it is restricted to members of the class.

```php
<?php
class User
{
    /** @var String $name */
    private $name;

    /** @var String $gender */
    private $gender;

    /** @return String */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param type $name
     * @return \user
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /** @return String */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param String $gender
     * @return \user
     * @throws \Exception
     */
    public function setGender($gender)
    {
        if ('male' !== $gender and 'female' !== $gender) {
            throw new \Exception('Set male or female for gender');
        }

        $this->gender = $gender;

        return $this;
    }
}

$user = new User();
$user->setName('saman');
$user->setGender('male');
// An exception will throw and you can not set 'Y' to user gender
// $user->setGender('Y');
var_dump($user->getGender());
?>
```

##### 6) Where session is being stored?

- A session is a way to store information (in variables) to be used across multiple pages.

- The session data that you read and write using $_SESSION is stored on server side, usually in text files in a temporary directory. They can not be accessed from outside.

- The thing connecting a session to a client browser is the session ID, which is usually stored in a cookie (see the comments for exceptions to that rule). This ID is, and should be, the only thing about your session that is stored on client side.

- If you delete the cookie in the browser, the connection to that session is lost, even if the file on the server continues to exist for some time.

- The session.save_path variable influences the location on the server where the session data is stored. If you are not the server's administrator, it is usually not necessary to change it.

##### 7) States of Ajax.

- 4 States:
1) Initial
2) Request
3) Process
4) Finished 

are the 4 states of AJAX

##### 8) What is Prototype in javascript?

- The prototype property allows you to add properties and methods to an object.

```javascript
<script>

function employee(name,jobtitle,born)
{
this.name=name;
this.jobtitle=jobtitle;
this.born=born;
}

var fred=new employee("Fred Flintstone","Caveman",1970);
employee.prototype.salary=null;
fred.salary=20000;

document.write(fred.salary);

</script>
```

##### 9) How to optimize select query?

- By using indexes, query only required fields

##### 10) Index in MySQL.

- So, what is an index? Well, an index is a data structure (most commonly a B- tree) that stores the values for a specific column in a table. An index is created on a column of a table. So, the key points to remember are that an index consists of column values from one table, and that those values are stored in a data structure. The index is a data structure – remember that.
- Basically an index on a table works like an index in a book. 
- Indexes are used to find rows with specific column values quickly. Without an index, MySQL must begin with the first row and then read through the entire table to find the relevant rows. The larger the table, the more this costs. If the table has an index for the columns in question, MySQL can quickly determine the position to seek to in the middle of the data file without having to look at all the data. This is much faster than reading every row sequentially.

##### 11) MyISAM vs. Innodb

MYISAM:

MYISAM supports Table-level Locking
MyISAM designed for need of speed
MyISAM does not support foreign keys hence we call MySQL with MYISAM is DBMS
MyISAM stores its tables, data and indexes in diskspace using separate three different files. (tablename.FRM, tablename.MYD, tablename.MYI)
MYISAM not supports transaction. You cannot commit and rollback with MYISAM. Once you issue a command it’s done.
MYISAM supports fulltext search
You can use MyISAM, if the table is more static with lots of select and less update and delete.

INNODB:

InnoDB supports Row-level Locking
InnoDB designed for maximum performance when processing high volume of data
InnoDB support foreign keys hence we call MySQL with InnoDB is RDBMS
InnoDB stores its tables and indexes in a tablespace
InnoDB supports transaction. You can commit and rollback with InnoDB

##### 12) What is Polymorphism?

- Polymorphism in object oriented programming is a mechanism in which classes have different functionality while sharing a common interface.
- It can be achieved using Interfaces.

##### 13) Interface.

- An interface is similar to a class except that it cannot contain code. An interface can define method names and arguments, but not the contents of the methods.
- Any classes implementing an interface must implement all methods defined by the interface

##### 14) Abstract Class

- An abstract class is a mix between an interface and a class. It can define functionality as well as interface (in the form of abstract methods). 
- Classes extending an abstract class must implement all of the abstract methods defined in the abstract class.

##### 15) Set ini values.

```php
- ini_set('max_execution_time', 300);
- ini_set('upload_max_filesize', 2M);
```

##### 16) Serialize, Unserialize in PHP.

- Serialize() converts an array, given as its only parameter, into a normal string that you can save in a file, pass in a URL, etc. Unserialize() is the opposite of serialize() - it takes a serialize()d string and converts it back to an array.
- Generates a storable representation of a value. This is useful for storing or passing PHP values around without losing their type and structure.

##### 17) Form with file input

```html
<form enctype="multipart/form-data">
```

##### 18) Master value and Local value in phpinfo()

- the "master" value is the one set up in the configuration, whereas the local value is the one that is currently in effect. 
- For example, if you've used ini_set() before calling phpinfo(), you'll change the master value, and the new setting will be shown in the local value column.

##### 19) What is `final` class?

- A final class is one which cannot be extended http://www.php.net/manual/en/language.oop5.final.php
- You would use it where the class contained methods which you specifically do not want overridden. This may be because doing do would break your application in some way.

##### 20) What is `final` method?

- `final` methods are methods which we don't want to be overridden by child classes.

##### 21) REST vs. SOAP

- Both methods are used by many of the large players. It's a matter of preference. My preference is REST because it's simpler to use and understand.

**SOAP:**

* SOAP builds an XML protocol on top of HTTP or sometimes TCP/IP.
* SOAP describes functions, and types of data.
* SOAP is a successor of XML-RPC and is very similar, but describes a standard way to communicate.
* Several programming languages have native support for SOAP, you typically feed it a web service URL and you can call its web service functions without the need of specific code.
* Binary data that is sent must be encoded first into a format such as base64 encoded.
* Has several protocols and technologies relating to it: WSDL, XSDs, SOAP, WS-Addressing

**Representational state transfer (REST):**

* REST need not be over HTTP but most of my points below will have an HTTP bias.
* REST is very lightweight, it says wait a minute, we don't need all of this complexity that SOAP created.
* Typically uses normal HTTP methods instead of a big XML format describing everything. For example to obtain a resource you use HTTP GET, to put a resource on the server you use HTTP PUT. To delete a resource on the server you use HTTP DELETE.
* REST is a very simple in that it uses HTTP GET, POST and PUT methods to update resources on the server.
* REST typically is best used with Resource Oriented Architecture (ROA). In this mode of thinking everything is a resource, and you would operate on these resources.
* As long as your programming language has an HTTP library, and most do, you can consume a REST HTTP protocol very easily.
* Binary data or binary resources can simply be delivered upon their request.

##### 22) What is "The Slow Query Log" ?

- The slow query log consists of SQL statements that took more than long_query_time seconds to execute. The minimum and default values of long_query_time are 1 and 10, respectively.
 
##### 23) What is "Explain" statement in MySQL?

- When you issue a query, the MySQL Query Optimizer tries to devise an optimal plan for query execution. You can see information about the plan by prefixing the query with EXPLAIN. EXPLAIN is one of the most powerful tools at your disposal for understanding and optimizing troublesome MySQL queries, but it’s a sad fact that many developers rarely make use of it.

```sql
EXPLAIN SELECT * FROM categoriesG
```

##### 24) Window.onload vs document.ready ?

- The ``ready`` event occurs after the HTML document has been loaded, while the onload event occurs later, when all content (e.g. images) also has been loaded.

- The ``onload`` event is a standard event in the DOM, while the ready event is specific to jQuery. The purpose of the ``ready`` event is that it should occur as early as possible after the document has loaded, so that code that adds funcionality to the elements in the page doesn't have to wait for all content to load.

##### 25) Block and Inline elements of HTML

##### 26) Types of trigger in MySQL

##### 27) CSS vs CSS3

##### 28) Advantages of jQuery over Javascript

##### 29) Is Javascript case-sensitive?

##### 30) What is jQuery? (In depth)
